var users = [
    {
        firstName: 'Anastasiya',
        lastName: 'Figol',
        birthDay: '02.06.1900',
        phones: [
            '+79998887766',
            '+78887776655'
            ]
    },
    {
        firstName: 'Pirog',
        lastName: 'Pirogov',
        birthDay: '01.02.1902',
        phones: [
            '+79998887766',
            '+78887776655'
        ]
    }
];
function addUser (users) {
    var user = {};
    user.firstName = prompt('Введите имя');
    user.lastName = prompt('Введите фамилию');
    user.birthDay = prompt('Введите дату рождения');
    user.phones = [];
    while (true) {
        var phone = prompt('Номер телефона:(для выхода введите пустой номер)');
        if (!phone) break;
        user.phones.push(phone);
    }
    user.id = users.length;
    users.push(user);
}
function csvFormat(users) {
    return users.map(function(user) {
        return Object.keys(user).map(function(key) {
            if(typeof user[key] == 'string' || typeof user[key] == 'number') return '"' + user[key] + '"';
            else return user[key].map(function(phone) { return '"' + phone + '"'; }).join(';');
        }).join(';');
    }).join('\r\n');
}

function addIds (users) {
    for (var i = 0; i < users.length; i++) {
        users[i]['id'] = i;
    }
}

addIds(users);
addUser(users);

users.splice(2, 1);

for (var i = 0; i < users.length; i++ ) {
    console.log('имя: ' + users[i].firstName + ', ' + 'фамилия: ' + users[i].lastName + ', ' + 'др: ' + users[i].birthDay + ', ' + 'телефон: ' + users[i].phones);
}

console.log(csvFormat(users));