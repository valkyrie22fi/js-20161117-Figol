/* --- HW-5 --- */

// Task-1
// version-1
// var r = parseInt(prompt('red: '));
// var g = parseInt(prompt('green: '));
// var b = parseInt(prompt('blue: '));
//
// function convert (r, g, b) {
//     var r16 = r.toString(16);
//     var g16 = g.toString(16);
//     var b16 = b.toString(16);
//     return '#' + r16 + g16 + b16;
// }
// console.log(convert(r, g, b));
// // version-2
// var rgb = [
//     parseInt(prompt('red: ')),
//     parseInt(prompt('green: ')),
//     parseInt(prompt('blue: '))
// ];
// function convert (rgb) {
//     var color = '#';
//     for (var i = 0; i < rgb.length; i++) {
//         var part = rgb[i].toString(16);
//         if (part.length < 2 ) {
//             part = '0' + part;
//         }
//         color = color + part;
//     }
//     return color;
// }
// console.log(convert(rgb));

//Task-2
// var incorrect = true;
// var num = 0;
// while (incorrect) {
//     num = prompt('Введите число от 0 до 999: ');
//     if (num >= 0 && num <= 999) {
//         incorrect = false;
//     }
// }
// function numToObject (num) {
//     return {
//         'единицы': num % 10,
//         'десятки': Math.floor(num % 100 / 10),
//         'сотни': Math.floor(num / 100)
//     };
// }
// console.log(numToObject(num));

// function getUserNum(attempt) {
//     var maxAtt = 3;
//     if (attempt > maxAtt){
//         return false;
//     }
//     console.log('Попытка номер: ' + attempt);
//     var num = parseInt(prompt('Введите число от 0 до 999'));
//     if (isNaN(num) || num<0 || num>999) {
//         return getUserNum(attempt+1)
//     }
//     return num;
// }
// function numberToObject(arg) {
//     return {
//         'единицы': arg % 10,
//         'десятки': Math.floor(arg % 100 / 10),
//         'сотни': Math.floor(arg / 100)
//     };
// }
// var num = getUserNum(1);
// if (num === false) {
//     console.log('Превышено допустимое значение количества попыток ввода!');
// } else {
//     console.log(numberToObject(num));
// }

//Task-*
// function addPerson () {
//     var person = {};
//     person.user = prompt('Введите имя');
//     person.password = prompt('Введите пароль');
//     person.id = prompt('Введите id');
//     return person;
// }
// function objectToQueryString (person) {
//     console.log('User=' + person.user + '&' + 'Password=' + person.password + '&' + 'ID=' + person.id);
// }
// var person = addPerson();
// var person2 = addPerson();
//
// objectToQueryString(person);
// objectToQueryString(person2);

/* --- HW-4 --- */

// Task-1
// // version-1
// function min (x, y) {
//     if (x > y) {
//         return y;
//     } else {
//         return x;
//     }
// }
// console.log (min(0, 10));
// console.log (min(0, -10));

// // version-2
//    console.log(Math.min(0,10));
//    console.log(Math.min(0,-10));

// Task-2
// function countBs (string, simbolFind) {
//     var length = string.length - 1;
//     var count = 0;
//     for (var i = 0; i <= length; i++) {
//         var simbol = string.charAt(i);
//         if (simbol == simbolFind) {
//             count = count + 1;
//         }
//     }
//     return 'Букв ' + simbolFind + ': ' + count;
// }
// console.log(countBs('новый, совсем новый год', 'н'));


/* --- HW-3 --- */

// Task-1
// var x = " ";
// var y = "#";
// for(var i = 0; i < 8; i++){
//     x += y;
//     console.log(x);
// }

// Task-2
// //version-1
// var b = "#";
// var c = "_";
// var line = "";
// for ( var i = 0; i < 8; i++) {
//     if (i % 2 == 0) {
//         line = b + c + b + c + b + c + b + c;
//     } else {
//         line = c + b + c + b + c + b + c + b;
//     }
//     console.log(line);
//}
// //version-2
// var str = "";
// for ( var i = 1; i <= 8; i++) {
//     for (var k = 1; k<=4; k++) {
//         if (i % 2) {
//             str = str + "#_";
//         } else {
//             str = str + "_#";
//         }
//     }
//     str = str + '\n';
// }
// console.log(str);


// /* --- HW-2 --- */
//
// // Task-1
// var a = 1, b = 1, c, d;
// c = ++a; alert(c);  // 2 - 'префиксная форма'. Префиксная форма i++,возвращает новое значение.
// d = b++; alert(d);  // 1 - 'постфиксная форма'. Постфиксная форма i++,увеличивает значение, но возвращает старое значение,
//                     // бывшее до увеличения.
// c = (2+ ++a); alert(c); // 5 - сначала увеличивает 'а' до 3 (т.к. 'а' уже было равно 2), а затем производит операцию сложения
// d = (2+ b++); alert(d); // 4 - сначаа увеличивает 'а' до 3, но возвращает старое значение, затем производит операцию сложения
// alert(a); // 3 - выводит полученное значение в предыдущей 'префиксной форме'
// alert(b); // 3 - выводит полученное значение в предыдущей 'постфиксной форме'
//
// // Task-2
// var y = 2;
// var x = 1 + (y *= 2); // (y *= 2) == (y = y * 2), соответственно выражение равно: 4
// alert(x); // ответ == 5
//
// // Task-*
// // version-1
// for (var i = 1; i <= 100; i++) {
//     if ( i % 3 == 0 && i % 5 == 0) {
//         console.log('FizzBizz');
//     } else if ( i % 3 == 0 ) {
//         console.log('Fizz');
//     } else if ( i % 5 == 0 ) {
//         console.log('Bizz');
//     } else {
//         console.log (i);
//     }
// }
// console.log('************'); ///разделяет два варианта
// // version-2
// for (var i = 1; i <= 100; i++) {
//     var output = "";
//     if ( i % 3 == 0 ) {
//         output = output + 'Fizz';
//     }
//     if ( i % 5 == 0 ) {
//         output = output + 'Bizz';
//     }
//     if ( output == "" ) {
//         output = i;
//     }
//     console.log(output);
// }

// /* --- HW-1 --- */
// alert('Привет, Javascript');
// var name = 'Василий';
// var admin = name;
// alert(admin);